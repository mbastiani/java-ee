package br.com.fgp.arquitetura.software.java.rest;


import br.com.fgp.arquitetura.software.java.model.Estado;
import br.com.fgp.arquitetura.software.java.persistence.EstadoPersist;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@Path("estado")
public class EstadoResource {

    @Inject
    private EstadoPersist estadoPersist;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Estado> getJson() {
        return estadoPersist.listar();
    }

}
