package br.com.fgp.arquitetura.software.java.persistence;

import br.com.fgp.arquitetura.software.java.model.Estado;
import br.com.fgp.arquitetura.software.java.model.Evento;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Named
public class EstadoPersist {

    @PersistenceContext
    private EntityManager entityManager;

    public void novoEstado(Estado estado) {
        entityManager.persist(estado);
    }

    public List<Estado> listar() {
        return entityManager.createNamedQuery("Estado.getAll", Estado.class).getResultList();
    }

}
