package br.com.fgp.arquitetura.software.java.model;

import javax.json.JsonObject;
import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "Estado.getAll", query = "select e from Estado e")
})
public class Estado {

    @Id
    @GeneratedValue
    private long id;
    private String nome;
    private String codigoIbge;
    private String observacao;

    public Estado() {
    }

    public Estado(JsonObject jsonObject) {
        this.nome = jsonObject.getString("nome");
        this.codigoIbge = jsonObject.getString("codigoIbge");
        this.observacao = jsonObject.getString("observacao");
    }

    public Estado(String nome, String codigoIbge, String observacao) {
        this.nome = nome;
        this.codigoIbge = codigoIbge;
        this.observacao = observacao;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(String codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

}
