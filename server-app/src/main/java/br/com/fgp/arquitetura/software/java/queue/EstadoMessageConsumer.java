package br.com.fgp.arquitetura.software.java.queue;

import br.com.fgp.arquitetura.software.java.model.Estado;
import br.com.fgp.arquitetura.software.java.persistence.EstadoPersist;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

@MessageDriven(mappedName = "queue/PersistEstadoQueue",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        }
)
public class EstadoMessageConsumer implements MessageListener {

    @Inject
    private EstadoPersist estadoPersist;

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;

            JsonReader jsonReader = Json.createReader(new StringReader(textMessage.getText()));
            JsonObject jsonObject = jsonReader.readObject();

            estadoPersist.novoEstado(new Estado(jsonObject));
        } catch (JMSException ex) {
            Logger.getLogger(EstadoMessageConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
