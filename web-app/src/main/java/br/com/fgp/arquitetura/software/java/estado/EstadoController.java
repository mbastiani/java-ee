package br.com.fgp.arquitetura.software.java.estado;

import br.com.fgp.arquitetura.software.java.MessageProducer;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class EstadoController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private MessageProducer messageProducer;

    @Inject
    @RestClient
    private EstadoRest estadoRest;

    private String nome;
    private String codigoIbge;
    private String observacao;

    public void criarEstado() {
        JsonObject json = Json.createObjectBuilder()
                .add("nome", nome)
                .add("codigoIbge", codigoIbge)
                .add("observacao", observacao)
                .build();
        messageProducer.sendMessageEstado(json);

        addMessage("Estado salvo com sucesso");
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<Map<String, Object>> getEstados() {
        return estadoRest.estados();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(String codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

}
