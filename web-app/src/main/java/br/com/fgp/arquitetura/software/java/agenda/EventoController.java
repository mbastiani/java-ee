package br.com.fgp.arquitetura.software.java.agenda;

import br.com.fgp.arquitetura.software.java.MessageProducer;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@Named
@RequestScoped
public class EventoController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private MessageProducer messageProducer;

    @Inject
    @RestClient
    private EventoRest eventoRest;

    private String data;
    private String nome;
    private String descricao;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void criarEvento() {
        JsonObject json = Json.createObjectBuilder()
                .add("data", data)
                .add("nome", nome)
                .add("descricao", descricao)
                .build();
        messageProducer.sendMessageEvento(json);

        addMessage("Evento salvo com sucesso");
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<Map<String, Object>> getEventos() {
        return eventoRest.eventos();
    }

}
