package br.com.fgp.arquitetura.software.java.agenda;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */

@Path("evento")
@RegisterRestClient(baseUri = "http://localhost:8080/server/rest")
public interface EventoRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Map<String, Object>> eventos();

}
